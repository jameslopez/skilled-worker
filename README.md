# Skilled Worker v0.1 Beta

Prototype that matches workers with certain skills to customers.

### Installation

Run
```sh
$ git clone git@bitbucket.org:bluegod1/skilled-worker.git
$ cd skilled-worker
$ bundle install
$ cp config/secrets.example.yml config/secrets.yml
```
Setup a postgreSQL DB - You can follow:
https://www.digitalocean.com/community/tutorials/how-to-setup-ruby-on-rails-with-postgres

Edit config/secrets.yml to adapt your DB settings. The default DBs are called skilled_worker_development and skilled_worker_test by default.

You can run the rails app locally with:

```sh
$ rails s
```
### Data setup
You can load 100 random skills and 1000 fake workers by default as follows (in this order):

```sh
$ rake db:seed:skills[100]
$ rake db:seed:workers[1000]
```

### Basic UI

http://localhost:3000 should redirect you to the /skills page. You can then sign up and log in as a customer. In the skills page you can select the skills and the matching workers would appear - they will have a link to associate that worker with the customer logged in.

You can then log out (top right corner) and log in as a worker and select a skill that will get associated with your profile. At the moment a worker wouldn't be able to create new skills but use existing ones.

### Tests

Simply run:

```sh
$ rspec spec
```

### Caveats

Not much attention has been put to the frotend / views / controllers so interface might be a bit buggy and not very user friendly - but should be enough to test the functionality. Things can certainly be improved and refactored there.

Take this app as a prototype that should give a good insight of the actual way of coding.

License
----

MIT

Copyright 2015 James Lopez
