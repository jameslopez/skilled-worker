class AddUniqueIndexToCustomerWorkers < ActiveRecord::Migration
  def change
    remove_index :customers_workers, :name => 'index_customers_workers_on_customer_id_and_worker_id'
    add_index :customers_workers, [:customer_id, :worker_id], :unique => true
  end
end
