class CreateWorkersSkills < ActiveRecord::Migration
  def change
    create_table :workers_skills, id: false do |t|
      t.belongs_to :worker, index: true
      t.belongs_to :skill, index: true
    end
    add_index :workers_skills, [:worker_id, :skill_id]
  end
end
