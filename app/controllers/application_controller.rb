class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_user
    current_worker || current_customer
  end

  def auth_user!(opts = {})
    if customer_signed_in?
      authenticate_customer!
    else
      authenticate_worker!
    end
  end
end
