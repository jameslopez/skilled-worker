class WorkersController < ApplicationController
  before_action :auth_user!

  def index
    if customer_searching?
      @workers = workers
    end
    render 'index', layout: false
  end

  def customer
    worker = Worker.find(worker_id_params)
    render_not_found and return if !worker
    worker.customers << current_user
    render_worker_save(worker, worker.email)
  end

  def skill
    worker = current_worker
    render_not_found and return if !worker
    skill = Skill.find_or_create_by(name: workers_params[:skill])
    worker.skills << skill if skill
    render_worker_save(worker, skill.name)
  rescue ActiveRecord::RecordNotUnique
    render_error(worker)
  end

  def render_not_found
    render(text: "404 Not Found", status: 404)
  end

  private

  def workers
    Search::WorkerWithSkillFinder.find(keywords: keywords)
      .page workers_params[:workers_page]
  end

  def customer_searching?
    workers_params[:keywords] && current_user.customer?
  end

  def worker_id_params
    params.permit(:id)[:id]
  end

  def workers_params
    @_workers_params ||= params.permit(:workers_page, :keywords, :skill)
  end

  def keywords
    workers_params[:keywords].split(',') if workers_params[:keywords]
  end

  def render_worker_save(worker, item)
    if worker.save
      render_success(item)
    else
      render_error(worker)
    end
  end

  def render_success(item)
    render(
      text: "Added #{item} to #{current_user.email}!",
      status: 200, layout: false)
  end

  def render_error(worker)
    render(
      text: "Error! #{worker.errors.full_messages}",
      status: 422, layout: false)
  end
end
