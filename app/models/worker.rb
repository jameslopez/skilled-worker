class Worker < User
  has_and_belongs_to_many :skills, join_table: :workers_skills
  has_and_belongs_to_many :customers, join_table: :customers_workers
  paginates_per 3

  def image_url
    "https://robohash.org/#{self.email}?set=set2&size=80x80"
  end
end
