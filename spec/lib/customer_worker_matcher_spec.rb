require 'rails_helper'

describe Match::CustomerWorkerMatcher, type: :feature do
  let(:customer1) { create(:customer) }
  let!(:skill1) { create(:skill, name: 'ironing') }
  let!(:worker1) { create(:worker, skills: [skill1]) }

  it 'match customer 1 with worker 2' do
    expect{ described_class.match!(customer: customer1, worker: worker1)}
      .to change { CustomersWorkers.count } .from(0).to(1)
  end
end

class CustomersWorkers < ActiveRecord::Base
  self.table_name = 'customers_workers'
end