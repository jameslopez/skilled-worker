require 'faker'

namespace :db do
  namespace :seed do
    desc 'create random skills'
    task :workers, [:number] => [:environment] do |t, args|
      Faker::Config.locale = 'en-GB'
      args.number.to_i.times do |i|
        Worker.create!(
          email: Faker::Internet.email,
          password: "password",
          password_confirmation: "password",
          skills: Skill.all.sample(2)
        )
      end
    end
  end
end
