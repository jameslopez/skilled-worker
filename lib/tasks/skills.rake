require 'faker'

namespace :db do
  namespace :seed do
    desc 'create random skills'
    task :skills, [:number] => [:environment] do |t, args|
      @max_attempts = 5
      Faker::Config.locale = 'en-GB'
      args.number.to_i.times do |i|
        @retries = 0
        create_skill
      end
    end
  end
end

private

def create_skill
  begin
    @retries += 1
    Skill.create!(
      name: Faker::Name.title
    )
  rescue RecordInvalid
    create_skill unless @retries > @max_attempts
  end
end
