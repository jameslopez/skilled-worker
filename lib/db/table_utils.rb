module Db::TableUtils
  extend self

  private

  def workers
    @_workers ||= Worker.arel_table
  end

  def skills
    @_skills ||= Skill.arel_table
  end

  def workers_skills
    @_workers_skills ||= Arel::Table.new(:worker_skills).alias('ws')
  end
end